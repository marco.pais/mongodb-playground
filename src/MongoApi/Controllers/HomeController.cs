﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Core.Models;
using Core.Services;

namespace MongoPlaygroundApi.Controllers
{
    [RoutePrefix("")]
    public class HomeController : ApiController
    {
        [Route("mmap/summary")]
        public async Task<Metric> SummaryMmap()
        {
            var mmapDataService = new MMAPService();

            var summary = await mmapDataService.Summary();

            return summary;
        }

        [Route("wt/summary")]
        public async Task<Metric> SummaryWt()
        {
            var wtDataService = new WiredTigerService();

            var summary = await wtDataService.Summary();

            return summary;
        }

        [Route("mmap/read")]
        public async Task<IList<Quote>> ReadMmap([FromBody]ReadRequest request)
        {
            var wtDataService = new WiredTigerService();

            var quote = await wtDataService.GetQuotes(request);

            return quote;
        }

        [Route("wt/read")]
        public async Task<IList<Quote>> ReadWt([FromBody]ReadRequest request)
        {
            var wtDataService = new WiredTigerService();

            var quote = await wtDataService.GetQuotes(request);

            return quote;
        }

        [HttpPost]
        [Route("mmap/write")]
        public async void WriteMmap([FromBody]int numberOfObjects)
        {
            var wtDataService = new WiredTigerService();

            await wtDataService.Save(numberOfObjects);
        }

        [HttpPost]
        [Route("wt/write")]
        public async void WriteWt([FromBody]int numberOfObjects)
        {
            var wtDataService = new WiredTigerService();

            await wtDataService.Save(numberOfObjects);
        }
    }
}

﻿
namespace Core.Models
{
    public class User
    {
        public string Name;
        public string Address;
        public string Email;
        public string Phone;
        public int ElectricityUsage;
        public int ElectricitySpend;
        public int GasUsage;
        public int GasSpend;
    }
}

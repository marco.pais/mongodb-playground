﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;

namespace Core.Models
{
    public class Quote
    {
        public ObjectId Id;
        public DateTime Timestamp;
        public User User;
        public string Channel;
        public IList<Deal> Deals;
        public int Rnd;
    }
}

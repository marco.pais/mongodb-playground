﻿
namespace Core.Models
{
    public class Deal
    {
        public int Id;
        public string Name;
        public string Description;
        public int FuelType;
        public decimal Saving;
        public int Score;
        public Supplier Supplier;
    }
}

﻿
namespace Core.Models
{
    public class Supplier
    {
        public string Name;
        public string Email;
        public string Phone;
    }
}

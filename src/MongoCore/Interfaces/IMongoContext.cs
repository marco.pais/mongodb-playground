﻿using Core.Models;
using MongoDB.Driver;

namespace Core.Interfaces
{
    public interface IMongoContext
    {
        void Init(string cnKey);

        IMongoCollection<Quote> Quotes { get; }

        IMongoCollection<Quote> Checkouts { get; }

        IMongoCollection<Metric> Metrics { get; }
    }
}

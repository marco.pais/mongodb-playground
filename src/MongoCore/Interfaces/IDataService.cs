﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Models;

namespace Core.Interfaces
{
    public interface IDataService
    {
        Task Save(int numberOfObjects);

        Task<IList<Quote>> GetQuotes(ReadRequest request);

        Task<Quote> GetCheckouts(ReadRequest request);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Core.Interfaces;
using Core.Models;
using MongoDB.Driver;

namespace Core.Services
{
    public class DataService : IDataService
    {
        protected readonly IMongoContext Context;

        protected DataService() { }

        public DataService(IMongoContext context)
        {
            Context = context;
            
        }

        public async Task<Metric> Summary()
        {
            return new Metric();
        }

        public async Task Save(int numberOfObjects)
        {
            var quotes = new List<Quote>();

            for (int i = 0; i < numberOfObjects; i++)
            {
                quotes.Add(SampleQuote());
            }

            var stopwatch = Stopwatch.StartNew();

            await Context.Quotes.InsertManyAsync(quotes);

            stopwatch.Stop();

            var metric = new Metric();
            metric.Operation = "write";
            metric.Duration = stopwatch.ElapsedMilliseconds;

            await Context.Metrics.InsertOneAsync(metric);
        }

        public async Task<IList<Quote>> GetQuotes(ReadRequest request)
        {
            var stopwatch = Stopwatch.StartNew();

            IList<Quote> quotes = await Context.Quotes.Find(_ => _.Rnd > request.RandomNumber).Limit(request.Limit).ToListAsync();

            stopwatch.Stop();

            var metric = new Metric();
            metric.Operation = "read";
            metric.Duration = stopwatch.ElapsedMilliseconds;

            await Context.Metrics.InsertOneAsync(metric);

            return quotes;
        }

        public async Task<Quote> GetCheckouts(ReadRequest request)
        {
            Quote quote = await Context.Checkouts.Find(_ => _.Rnd > request.RandomNumber).FirstOrDefaultAsync();

            return quote;
        }

        private Quote SampleQuote()
        {
            var quote = new Quote();

            quote.Timestamp = DateTime.Now;
            quote.User = new User();
            quote.User.Name = "John Doe";
            quote.User.Address = "Sample address";
            quote.User.Email = "john@example.com";
            quote.User.Phone = "123789456";
            quote.User.ElectricityUsage = 500;
            quote.User.ElectricitySpend = 40;
            quote.User.GasUsage = 500;
            quote.User.GasSpend = 40;
            quote.Channel = "Sample channel";

            var deal = new Deal();
            deal.Id = 1;
            deal.Name = "Sample deal";
            deal.Description = "Sample deal description";
            deal.FuelType = 1;
            deal.Saving = 50;
            deal.Score = 1;
            deal.Supplier = new Supplier();
            deal.Supplier.Name = "Sample supplier";
            deal.Supplier.Email = "supplier@suppliers.com";
            deal.Supplier.Phone = "321654789";

            quote.Deals = new List<Deal>();
            quote.Deals.Add(deal);

            return quote;
        }
    }

    public class MMAPService : DataService
    {
        public MMAPService()
        {
            Context.Init("MongoMMAP");
        }
    }

    public class WiredTigerService : DataService
    {
        public WiredTigerService()
        {
            Context.Init("MongoWT");
        }
    }
}

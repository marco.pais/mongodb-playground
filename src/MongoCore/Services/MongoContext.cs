﻿using System.Configuration;
using Core.Interfaces;
using Core.Models;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;

namespace Core.Services
{
    public class MongoContext : IMongoContext
    {
        const string DatabaseName = "energy";
        const string QuotesCollection = "quotes";
        const string CheckoutsCollection = "checkouts";
        const string MetricsCollection = "reports";

        private IMongoDatabase _database;

        public MongoContext()
        {
            var conventionPack = new ConventionPack();

            conventionPack.Add(new CamelCaseElementNameConvention());

            ConventionRegistry.Register("camelCase", conventionPack, t => true);
        }

        public void Init(string cnKey)
        {
            var connectionString = ConfigurationManager.ConnectionStrings[cnKey].ConnectionString;

            IMongoClient client = new MongoClient(connectionString);

            _database = client.GetDatabase(DatabaseName);
        }

        public IMongoCollection<Quote> Quotes
        {
            get { return _database.GetCollection<Quote>(QuotesCollection); }
        }

        public IMongoCollection<Quote> Checkouts
        {
            get { return _database.GetCollection<Quote>(CheckoutsCollection); }
        }

        public IMongoCollection<Metric> Metrics
        {
            get { return _database.GetCollection<Metric>(MetricsCollection); }
        }
    }
}

﻿var comms = {
    post: ajaxPost,
    get: ajaxGet
};

var baseUrl = $('#api-url').data('search-url');

function ajaxPost(url, data) {
    
    var ajaxOptions = {
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        data: data
    };
    
    return $.ajax(baseUrl + url, ajaxOptions);
}

function ajaxGet(url, params) {

    var strMethodUrl = url + '?' + params;

    var options = {
        type: "GET",
        contentType: "application/json"
    };

    return $.ajax(strMethodUrl, options);
}
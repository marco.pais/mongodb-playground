﻿var btnWrite = $("#btn-write");
var btnWriteDefaultText = $("#btn-write").text();
var btnWriteExecutingText = "Executing... ";
var defaultDuration = 15000;
var duration = countdown = defaultDuration;
var repeat = 1000;
var timer;

var write = function () {

    btnWrite.text(btnWriteExecutingText + Math.ceil((countdown) / 1000));
    countdown -= repeat;

    comms.post("aa");

} 

var startwriting = function () {
    btnWrite.addClass("disabled");

    setTimeout(function() {
        clearInterval(timer);
        btnWrite.removeClass("disabled").text(btnWriteDefaultText);
        duration = countdown = defaultDuration;
    }, duration);

    write();

    timer = setInterval(write, repeat);
}

btnWrite.on("click", startwriting);


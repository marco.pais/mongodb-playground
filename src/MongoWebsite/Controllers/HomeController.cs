﻿using System.Web.Mvc;

namespace MongoPlayground.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "This is the summary";


            return View();
        }

        public ActionResult WriteRead()
        {
            ViewBag.Message = "This is the write & read";

            return View();
        }

        public ActionResult Indexes()
        {
            ViewBag.Message = "This is the indexes";

            return View();
        }

        public ActionResult Driver()
        {
            ViewBag.Message = "This is the driver";

            return View();
        }
    }
}
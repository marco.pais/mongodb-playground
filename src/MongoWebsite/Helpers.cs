﻿using System.Configuration;
using System.Web.Mvc;

namespace MongoPlayground
{
    public static class Helpers
    {
        public static MvcHtmlString ConfigurationSetting(this HtmlHelper html, string description)
        {
            string configuration = ConfigurationManager.AppSettings[description];

            var tag = new TagBuilder("div");
            tag.Attributes.Add("id", description.Replace('.', '-'));
            tag.Attributes.Add("data-search-url", configuration);

            return new MvcHtmlString(tag.ToString());
        }
    }
}
